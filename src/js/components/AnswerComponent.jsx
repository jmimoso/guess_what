import React, { Component } from 'react';
import ReactHowler from 'react-howler';

export default class AnswerComponent extends Component {

	constructor(props) {
		super(props);
		this.state = {
			status: '',
			answer: props.answer,
			_answersSelected: false
		}
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			answer: nextProps.answer,
			_answersSelected: nextProps._answersSelected
		});
	}

	render() {
		return (
			<li className={'answer-wrapper ' + this.state.status} onClick={ev => this.selectAnswer(ev)}>
				<div className='text-container'>
					<p className='text'>
						{this.state.answer.text}
					</p>
				</div>
				<div className='number-container'>
					<div className="number-image-container">
						<img className='number-image' src={this.setImage()} />
					</div>
					<p className='number'>
						{this.setNumber()}
					</p>
				</div>
			</li>
		);
	}

	selectAnswer = (ev) => {
		if(!this.state._answersSelected) {
			this.props._setAnswersSelected();
			this.setAnswerStatus();
		} else {
			ev.preventDefault();
		}
	}

	setAnswerStatus() {
		if(this.props.answer.correct) {
			this.setState({
				status: 'correct'
			}, () => {
				this.props._setAudioPlaying(this.state.status);
			});
			setTimeout(() => {
				this.setState({
					status: ''
				});
			}, 1500);
			this.props._nextQuestion();
		} else if(!this.props.answer.correct) {
			this.setState({
				status: 'incorrect'
			}, () => {
				this.props._setAudioPlaying(this.state.status);
			});
			setTimeout(() => {
				this.setState({
					status: ''
				});
			}, 1500);
			this.props._openRestartGameModal();
		}
		
	}

	setNumber() {
		switch(this.props.index) {
		    case 0:
		        return 'A';
		        break;
		    case 1:
		        return 'B';
		        break;
		    case 2:
		        return 'C';
		        break;
		    case 3:
		        return 'D';
		        break;
		}
	}

	setImage() {
		switch(this.state.status) {
		    case 'correct':
		        return require('../../assets/images/image_number_correct.png');
		        break;
		    case 'incorrect':
		        return require('../../assets/images/image_number_incorrect.png');
		        break;
		    default:
		        return require('../../assets/images/image_number.png');
		}
	}


}
import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import ReactHowler from 'react-howler';

import { FaImage, FaClose, FaChevronLeft } from 'react-icons/lib/fa';

import AnswerComponent from '../components/AnswerComponent';

import correctAudio from '../../assets/audios/correct.mp3';
import incorrectAudio from '../../assets/audios/incorrect.mp3';


class GameController extends Component {

	constructor(props) {
		super(props);
		this.state = {
			newQuestions: props._newQuestions,
			answersSelected: false,
			imageModalOpen: false,
			restartModalOpen: false,
			playCorrect: false,
			playIncorrect: false
		};
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			newQuestions: nextProps._newQuestions
		});
	}

	render() {
		if(this.state.newQuestions) {
			return (
				<section className='game-container bg'>
					<div className='game-container-cell'>
						<div className='game-top-wrapper'>
							<div className='game-top-container'>
								<div className='rat-image-container'>
									<img className='rat-image' src={require('../../assets/images/image_rat.png')} />
								</div>
								<div className='image-button-container'>
									{this._renderImageButton()}
									<div className='bottom-line'></div>
								</div>
								<div className='text-balloon-container'>
									<div className='text-balloon'>
										<p className='text'>
											{this.state.newQuestions[0].question}
										</p>
										<div className='currentquestion'>
											{this._renderProgress()}
										</div>
									</div>
								</div>
							</div>
						</div>
						<div className='game-bottom-wrapper'>
							<ul className='answers-container'>
								{this._renderAnswers()}
							</ul>
						</div>
						<div className='back-button-container'>
							<button className='back-button' onClick={this._goBack}>
								<FaChevronLeft size={26} color='#FFFFFF' />
							</button>
						</div>
					</div>
					<div className='image-modal-container' style={this.state.imageModalOpen ? {display: 'block'} : {display: 'none'}}>
						<div className='image-modal-content'>
							<div className='image-modal'>
								<div className="image" style={this.setBackgroundImage()}></div>
							</div>
							<div className='modal-button-container'>
								<button className='modal-button' onClick={this._setModalStatus}>
									<FaClose size={26} color='#FFFFFF' />
								</button>
							</div>
						</div>
					</div>
					<div className='restart-modal-container' style={this.state.restartModalOpen ? {display: 'table'} : {display: 'none'}}>
						<div className='restart-modal-content'>
							<div className='modal-wrapper'>
								<div className='top-block'>
									<p className='text'>Play Again?</p>
								</div>
								<div className='bottom-block'>
									<div className='modal-button-wrapper'>
										<button className='modal-button' onClick={this.restartGame}>
											Yes
										</button>
									</div>
									<div className='modal-button-wrapper'>
										<button className='modal-button' onClick={this._goBack}>
											No
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className='bottom-line-offside'></div>
					<ReactHowler playing={this.state.playCorrect} src={correctAudio} onEnd={this.stopAudio} />
					<ReactHowler playing={this.state.playIncorrect} src={incorrectAudio} onEnd={this.stopAudio} />
				</section>
			);
		} else {
			return (
				<section className='game-container bg'></section>
			);
		}
		
	}

	_renderAnswers() {
		return this.state.newQuestions[0].answers.map((answer, key) => {
			return (
				<AnswerComponent 
					index={key} 
					key={key} 
					answer={answer}
					_setAnswersSelected={this.setAnswersSelected}
					_answersSelected={this.state.answersSelected}
					_nextQuestion={this.nextQuestion}
					_openRestartGameModal={this.openRestartGameModal}
					_setAudioPlaying={this.setAudioPlaying}>
				</AnswerComponent>
			);
		});
	}

	_renderImageButton() {
		if(this.state.newQuestions[0].image != '') {
			return (
				<button className='image-button' onClick={this._setModalStatus}>
					<FaImage size={26} color='#FFFFFF' />
				</button>
			);
		}
		return null;
	}

	_renderProgress() {
		return (
			<p className='text'>
				{(this.props.questions.length - this.state.newQuestions.length + 1)  + '/' + this.props.questions.length}
			</p>
		);
	}

	_setModalStatus = () => {
		this.setState({
			imageModalOpen: !this.state.imageModalOpen
		});
	}

	_goBack = () => {
		this.props.history.push('/');
	}

	_goToResult = () => {
		this.props.history.push('/result');
	}

	setAnswersSelected = () => {
		this.setState({
			answersSelected: true
		});
	}

	nextQuestion = () => {
		var newQuestions = this.state.newQuestions.slice(1);
		if(this.state.newQuestions.length === 1) {
			setTimeout(() => {
				this._goToResult();
			}, 1500);
		} else {
			setTimeout(() => {
				this.props._scrollTop();
				this.setState({
					answersSelected: false,
					newQuestions: newQuestions
				});
			}, 1500);
		}
	}

	restartGame = () => {
		this.props._scrollTop();
		this.setState({
			restartModalOpen: !this.state.restartModalOpen,
			answersSelected: false
		});
		this.props._fetchQuestions();
	}

	openRestartGameModal = () => {
		setTimeout(() => {
			this.setState({
				restartModalOpen: !this.state.restartModalOpen
			});
		}, 1500);
	}

	setBackgroundImage() {
		var imageUrl;
		if(this.state.newQuestions[0].image != '') {
			imageUrl = require('../../assets/images/questions/' + this.state.newQuestions[0].image);
		} else {
			imageUrl =  '';
		}
		return {
			backgroundImage: `url("${imageUrl}")`
		};
	}

	setAudioPlaying = (audio) => {
		if(audio === 'correct') {
			this.setState({
				playCorrect: true
			});
		} else {
			this.setState({
				playIncorrect: true
			});
		}
	}

	stopAudio = () => {
		this.setState({
			playCorrect: false,
			playIncorrect: false
		})
	}


}

export default withRouter(GameController);
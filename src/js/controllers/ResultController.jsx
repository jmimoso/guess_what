import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';


class ResultController extends Component {

	constructor(props) {
		super(props);
	}

	render() {
		return (
			<section className='result-container bg'>
				<div className='result-container-cell'>
					<div className="result-image-container">
						<img className='result-image' src={require('../../assets/images/image_result.png')} />
					</div>
					<div className='result-button-container'>
						<span className='text'>Play Again?</span>
						<button className='result-button' onClick={this.restartGame}>
							<span>Yes</span>
						</button>
						<Link to='/' className='result-button'>
							<span>No</span>
						</Link>
					</div>
				</div>
			</section>
		);
	}

	restartGame = () => {
		this.props._setNewQuestions();
		this.props.history.push('/game');
	}

}

export default withRouter(ResultController);
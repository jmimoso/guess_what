import React, { Component } from 'react';
import { Link } from 'react-router-dom';


export default class EntryController extends Component {

	constructor(props) {
		super(props);
	}

	render() {
		return (
			<section className='entry-container bg'>
				<div className='entry-container-cell'>
					<div className="logo-image-container">
						<img className='logo-image' src={require('../../assets/images/entry_logo.png')} />
					</div>
					<div className='start-button-container'>
						<Link to='/game' className='start-button'>
							<span>Start</span>
						</Link>
					</div>
					<img className='footer-logo-image' src={require('../../assets/images/footer_logo.png')} />
				</div>
			</section>
		);
	}

}
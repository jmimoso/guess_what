import React, { Component } from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import 'whatwg-fetch';

import EntryController from './EntryController';
import GameController from './GameController';
import ResultController from './ResultController';


import { detect } from 'detect-browser';
const browser = detect();

export default class AppController extends Component {

	constructor(props) {
		super(props);
		this.state = {
		}
	}

	componentWillMount() {
		this.fetchQuestions();
	}

	render() {
		return (
			<Router>
				<div ref='mainContent' id='mainContent' className='main-content' style={this.setBackgroundImage()}>
					<Switch>
						<Route exact path='/' render={() => <EntryController />} />
						<Route path='/game' render={() => <GameController questions={this.state.options} _scrollTop={this.scrollTop} _newQuestions={this.state.newQuestions} _fetchQuestions={this.fetchQuestions} />} />
						<Route path='/result' render={() => <ResultController _setNewQuestions={this.setNewQuestions} />} />
					</Switch>
				</div>
			</Router>
		);
	}

	fetchQuestions = () => {
		return fetch('./json/options.json')
        .then(response => {
        	return response.json()
        })
        .then(data => {
        	this.setState({
        		options: data
        	}, () => {
        		this.setNewQuestions();
        	});
        })
	}

	setNewQuestions = () => {
		this.setState({
			newQuestions: this.prepareQuestions(this.state.options)
		});
	}

	prepareQuestions = (questions) => {
		return questions.map(a => [Math.random(), a]).sort((a, b) => a[0] - b[0]).map(a => a[1]);
	}

	setBackgroundImage() {
		const imageUrl = require('../../assets/images/background.jpg');
		return {
			backgroundImage: `url("${imageUrl}")`
		};
	}

	scrollTop = () => {
		if(browser.name === 'chrome' || browser.name === 'firefox') {
			this.refs.mainContent.scrollTo(0, 0)
		}
	}

}